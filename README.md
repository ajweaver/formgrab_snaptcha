# FormGrab Snaptcha

## Introduction

This provides support for adding [Snaptcha](https://devot-ee.com/add-ons/snaptcha) to [FormGrab](https://brandnewbox.co.uk/products/details/formgrab) forms.

## Installation

Note: this extension requires that [Snaptcha](https://devot-ee.com/add-ons/snaptcha) is installed.

Copy the `formgrab_snaptcha` folder into your `system/user/addons` folder and install it from the ExpressionEngine Control Panel Add-ons page.

## Usage

### Creating a form

Create a form with FormGrab as usual but include the parameter:

	snaptcha="on"

in the form, eg:

	{exp:formgrab:form
		name="snaptcha_form"
		title="Snaptcha Form"
		snaptcha="on"
	}

	  <div class="form-group">
	    <label for="name">Name</label>
	    <input name="name" type="text" class="form-control" id="name" placeholder="Your name">
	  </div>
	  <div class="form-group">
	    <label for="email">Email address</label>
	    <input name="email" type="email" class="form-control" id="email" placeholder="Your email address">
	  </div>
	  <div class="form-group">
	    <label for="message">Message</label>
	    <textarea name="message" class="form-control" id="message" rows="3"></textarea>
	  </div>

	<button type="submit" class="btn btn-primary">Submit</button>

	</div>

	{/exp:formgrab:form}

## Limitations

2019-01-12

Currently it is possible to disable the snaptcha test by modifying the form before it is sent. This limitation will be removed when an update to FormGrab is released.

## Support

The recommended way to get help is to email <a href="mailto:support@brandnewbox.co.uk">support@brandnewbox.co.uk</a>.
