<?php

return array(
	'author'         => 'Andrew Weaver',
	'author_url'     => 'http://brandnewbox.co.uk/',
	'name'           => 'FormGrab Snaptcha',
	'description'    => 'Adds snaptcha to FormGrab forms',
	'version'        => '1.0.0',
	'namespace'      => 'Brandnewbox\Formgrab_snaptcha',
	'settings_exist' => FALSE
);