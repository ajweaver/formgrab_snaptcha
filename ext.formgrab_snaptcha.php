<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package     ExpressionEngine
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2003 - 2018, EllisLab, Inc.
 * @license     http://expressionengine.com/user_guide/license.html
 * @link        http://expressionengine.com
 * @since       Version 2.0
 * @filesource
 */

/**
 * FormGrab Snaptcha Extension
 *
 * @package    ExpressionEngine
 * @subpackage Addons
 * @category   Extension
 * @author     Andrew Weaver
 * @link       http://brandnewbox.co.uk/
 */
class Formgrab_snaptcha_ext
{
    public $settings       = array();
    public $description    = 'Adds snaptcha to FormGrab forms';
    public $docs_url       = '';
    public $name           = 'FormGrab Snaptcha';
    public $settings_exist = 'n';
    public $version        = '1.0.0';

    protected $snaptcha_installed = false;

    /**
     * Constructor
     *
     * @param   mixed Settings array or empty string if none exist.
     */
    public function __construct($settings = '')
    {
        $this->settings = $settings;
        $addons = ee('Addon')->installed();
        $this->snaptcha_installed = isset( $addons['snaptcha'] );
    }

    /**
     * Activate Extension
     *
     * This function enters the extension into the exp_extensions table
     *
     * @see http://codeigniter.com/user_guide/database/index.html for
     * more information on the db class.
     *
     * @return void
     */
    public function activate_extension()
    {
        // Setup custom settings in this array.
        $this->settings = array();

        ee()->db->insert_batch('extensions', array(
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_form_tagdata',
                'hook' => 'formgrab_form_tagdata',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_validate',
                'hook' => 'formgrab_validate',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
            array(
                'class' => __CLASS__,
                'method' => 'formgrab_save_submission',
                'hook' => 'formgrab_save_submission',
                'settings' => serialize($this->settings),
                'version' => $this->version,
                'enabled' => 'y',
            ),
        ));
    }

    function settings()
    {
        $settings = array();

        // General pattern:
        //
        // $settings[variable_name] => array(type, options, default);
        //
        // variable_name: short name for the setting and the key for the language file variable
        // type:          i - text input, t - textarea, r - radio buttons, c - checkboxes, s - select, ms - multiselect
        // options:       can be string (i, t) or array (r, c, s, ms)
        // default:       array member, array of members, string, nothing

        // $settings['site_key']      = array('i', '', "");
        // $settings['secret_key']      = array('i', '', "");

        return $settings;
    }

    /**
     * formgrab_form_tagdata Hook
     *
     * @param
     * @return
     */
    public function formgrab_form_tagdata( $tagdata )
    {
        $tagdata = ee()->extensions->last_call ? ee()->extensions->last_call : $tagdata;

        // Add snaptcha field to form

        if( ee()->TMPL->fetch_param('snaptcha') ) {

            if( $this->snaptcha_installed && file_exists( PATH_THIRD.'snaptcha/ext.snaptcha.php' ) ) {

                require_once PATH_THIRD.'snaptcha/ext.snaptcha.php';
                $Snaptcha = new Snaptcha_ext();
                $field = $Snaptcha->snaptcha_field();

                $tagdata .= $field;
                $tagdata .= '<input type="hidden" name="formgrab_snaptcha_ext" value="1" />';
            }
        }

        return $tagdata;
    }

    /**
     * formgrab_validate Hook
     *
     * @param
     * @return
     */
    public function formgrab_validate( $post )
    {
        $post = ee()->extensions->last_call ? ee()->extensions->last_call : $post;

        // todo: not ideal way to see if snaptcha should run
        if( isset( $post['formgrab_snaptcha_ext'] ) ) {
            if( $this->snaptcha_installed && file_exists( PATH_THIRD.'snaptcha/ext.snaptcha.php' ) ) {
                require_once PATH_THIRD.'snaptcha/ext.snaptcha.php';
                $Snaptcha = new Snaptcha_ext();

                $validated = $Snaptcha->snaptcha_validate();
                if( ! $validated ) {
                    $error = array();
                    $error[] = $Snaptcha->settings['error_message'];
                    return ee()->output->show_user_error('general', $error);
                }

                // Remove snaptcha fields from submission
                $snaptcha_field = $Snaptcha->settings['field_name'];
                foreach ($post as $key => $value)
                {
                    if (strpos($key, $snaptcha_field.'_') === 0)
                    {
                        unset( $post[$key] );
                    }
                }

            }
            unset( $post['formgrab_snaptcha_ext'] );
        }

        return $post;
    }

    /**
     * formgrab_save_submission Hook
     *
     * @param
     * @return
     */
    public function formgrab_save_submission( $submission, $post )
    {
        return $submission;
    }

    /**
     * Disable Extension
     *
     * This method removes information from the exp_extensions table
     *
     * @return void
     */
    public function disable_extension()
    {
        ee()->db->delete('extensions', array('class' => __CLASS__));
    }

    /**
     * Update Extension
     *
     * This function performs any necessary db updates when the extension
     * page is visited
     *
     * @return  mixed void on update / false if none
     */
    public function update_extension($current = '')
    {
        if ($current == '' OR $current == $this->version)
        {
            return FALSE;
        }

        ee()->db->update('extensions', array('version' => $this->version), array('class' => __CLASS__));
    }
}

/* End of file ext.formgrab_recaptcha.php */
/* Location: /system/expressionengine/third_party/formgrab_recaptcha/ext.formgrab_recaptcha.php */